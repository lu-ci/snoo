use reqwest;
use serde_json;
use crate::sub::Subreddit;
use std::result::Result;
use crate::error::Error;

#[allow(dead_code)]
pub struct SnooClient {
  subreddit: String,
  page: u32,
  category: String
}

#[allow(dead_code)]
impl SnooClient {
  pub fn new(subreddit: String) -> Self {
    Self{subreddit: subreddit, page: 0, category: "hot".to_owned()}
  }

  pub fn set_subreddit(&mut self, subreddit: String) {
    self.subreddit = subreddit;
  }

  pub fn set_page(&mut self, page: u32) {
    self.page = page;
  }

  pub fn set_category(&mut self, category: String) {
    self.category = category;
  }

  pub fn fetch(&self) -> Result<Subreddit, Error> {
    let url = format!("https://www.reddit.com/r/{}.json", self.subreddit);
    let bd = reqwest::get(&url)?.text()?;
    let des: Subreddit = serde_json::from_str(&bd)?;
    Ok(des)
  }
}
