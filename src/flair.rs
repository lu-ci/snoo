#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct LinkFlairRichtext {
  pub e: Option<String>,
  pub t: Option<String>
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct AuthorFlairRichtext {
  pub a: Option<String>,
  pub e: Option<String>,
  pub u: Option<String>
}