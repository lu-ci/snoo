#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct ResizedIcon {
  pub url: String,
  pub width: u32,
  pub height: u32
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Award {
  pub is_enabled: bool,
  pub count: u32,
  pub subreddit_id: Option<String>,
  pub description: String,
  pub coin_reward: u32,
  pub icon_width: u32,
  pub icon_url: String,
  pub days_of_premium: u32,
  pub icon_height: u32,
  pub resized_icons: Vec<ResizedIcon>,
  pub days_of_drip_extension: u32,
  pub award_type: String,
  pub coin_price: u32,
  pub id: String,
  pub name: String
}
