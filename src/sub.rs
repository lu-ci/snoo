use serde;
use crate::child::SubredditChild;

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct SubredditData {
  pub modhash: String,
  pub dist: u32,
  pub children: Vec<SubredditChild>,
  pub after: Option<String>,
  pub before: Option<String>
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Subreddit {
  pub kind: String,
  pub data: SubredditData
}
