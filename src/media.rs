#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Media {
  pub r#type: Option<String>,
  pub oembed: Option<OEmbed>,
  pub reddit_video: Option<RedditVideo>
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct MediaEmbed {
  pub content: Option<String>,
  pub width: Option<u32>,
  pub scrolling: Option<bool>,
  pub height: Option<u32>
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct OEmbed {
  pub provider_url: String,
  pub description: String,
  pub title: String,
  pub url: Option<String>,
  pub r#type: String,
  pub thumbnail_width: u32,
  pub height: u32,
  pub width: u32,
  pub html: String,
  pub version: String,
  pub provider_name: String,
  pub thumbnail_url: String,
  pub thumbnail_height: u32
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct RedditVideo {
  pub fallback_url: String,
  pub height: u32,
  pub width: u32,
  pub scrubber_media_url: String,
  pub dash_url: String,
  pub duration: u32,
  pub hls_url: String,
  pub is_gif: bool,
  pub transcoding_status: String
}