mod award;
mod child;
mod client;
mod error;
mod flair;
mod media;
mod sub;


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        // let examples = vec![
        //     "programmerhumor", "AzureLane", "hentai",
        //     "aww", "konosuba", "AdviceAnimals", "pics",
        //     "rareinsults", "anime_irl", "goddesses"
        // ];
        let examples = vec!["konosuba"];
        for sub_example in examples {
            println!("{}", sub_example);
            let client = crate::client::SnooClient::new(sub_example.to_string());
            let success = match client.fetch() {
                Ok(sd) => {
                    for child in sd.data.children {
                        println!("{}", child.data.title);
                    }
                    true
                },
                Err(why) => {
                    dbg!(why);
                    false
                }
            };
            assert!(success);
        }
    }
}
