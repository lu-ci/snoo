use crate::{
  flair::{
    AuthorFlairRichtext,
    LinkFlairRichtext
  },
  award::{
    Award
  },
  media::{
    Media,
    MediaEmbed
  }
};

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct SubredditChild {
  pub kind: String,
  pub data: ChildData
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
#[serde(untagged)]
pub enum ChangeStamp {
  Bool(bool),
  Float(f32)
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct ChildData {
  pub approved_at_utc: Option<String>,
  pub subreddit: String,
  pub selftext: String,
  pub author_fullname: Option<String>,
  pub saved: bool,
  pub mod_reason_title: Option<String>,
  pub gilded: u32,
  pub clicked: bool,
  pub title: String,
  pub link_flair_richtext: Vec<LinkFlairRichtext>,
  pub subreddit_name_prefixed: String,
  pub hidden: bool,
  pub pwls: Option<u32>,
  pub link_flair_css_class: Option<String>,
  pub downs: u32,
  pub thumbnail_height: Option<u32>,
  pub hide_score: bool,
  pub name: String,
  pub quarantine: bool,
  pub link_flair_text_color: String,
  pub author_flair_background_color: Option<String>,
  pub subreddit_type: String,
  pub ups: u32,
  pub total_awards_received: u32,
  pub media_embed: MediaEmbed,
  pub thumbnail_width: Option<u32>,
  pub author_flair_template_id: Option<String>,
  pub is_original_content: bool,
  pub secure_media: Option<Media>,
  pub is_reddit_media_domain: bool,
  pub is_meta: bool,
  pub category: Option<String>,
  pub secure_media_embed: MediaEmbed,
  pub link_flair_text: Option<String>,
  pub can_mod_post: bool,
  pub score: u32,
  pub approved_by: Option<String>,
  pub thumbnail: Option<String>,
  pub edited: ChangeStamp,
  pub author_flair_css_class: Option<String>,
  pub author_flair_richtext: Option<Vec<AuthorFlairRichtext>>,
  pub post_hint: Option<String>,
  pub content_categories: Option<Vec<String>>,
  pub is_self: bool,
  pub mod_note: Option<String>,
  pub created: f32,
  pub link_flair_type: String,
  pub wls: Option<u32>,
  pub banned_by: Option<String>,
  pub author_flair_type: String,
  pub domain: String,
  pub selftext_html: Option<String>,
  pub likes: Option<u32>,
  pub suggested_sort: Option<String>,
  pub banned_at_utc: Option<f32>,
  pub view_count: Option<u32>,
  pub archived: bool,
  pub no_follow: bool,
  pub is_crosspostable: bool,
  pub pinned: bool,
  pub over_18: bool,
  // pub preview: Preview, // TODO
  pub all_awardings: Vec<Award>,
  pub media_only: bool,
  pub can_gild: bool,
  pub spoiler: bool,
  pub locked: bool,
  pub author_flair_text: Option<String>,
  pub visited: bool,
  pub num_reports: Option<u32>,
  pub distinguished: Option<String>,
  pub subreddit_id: String,
  pub mod_reason_by: Option<String>,
  pub removal_reason: Option<String>,
  pub link_flair_background_color: String,
  pub id: String,
  pub is_robot_indexable: bool,
  pub report_reasons: Option<Vec<String>>,
  pub author: String,
  pub num_crossposts: u32,
  pub num_comments: u32,
  pub send_replies: bool,
  pub whitelist_status: Option<String>,
  pub contest_mode: bool,
  pub mod_reports: Vec<String>,
  pub author_patreon_flair: bool,
  pub author_flair_text_color: Option<String>,
  pub permalink: String,
  pub parent_whitelist_status: Option<String>,
  pub stickied: bool,
  pub url: String,
  pub subreddit_subscribers: u32,
  pub created_utc: f32,
  pub media: Option<Media>,
  pub is_video: bool
}